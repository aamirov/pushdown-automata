class PDAState:

    def __init__(self, self_transit=False, logic=None):
        self.logic = logic
        self.self_transit = self_transit
        self.transitions = set()

    def add_transitions(self, *others):
        for state in others:
            self.transitions.add(state)

    def can_transit(self, other):
        return other in self.transitions or self.self_transit and other is self

    def has_logic(self):
        return self.logic is not None

    def execute_logic(self, stack, *args):
        if self.has_logic():
            return self.logic(stack, *args)
        return False
