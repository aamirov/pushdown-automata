from enum import Enum


class Brackets(Enum):
    OPEN = 1
    CLOSE = -1


def open_bracket(stack, *args):
    stack.append((Brackets.OPEN, args[1]))
    return True


def close_bracket(stack, *args):
    if len(stack) == 0:
        return False
    top = stack.pop()
    args[0].append((top[1], args[1]))
    return top[0] == Brackets.OPEN
