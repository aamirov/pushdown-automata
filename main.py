from .state import PDAState
from .pda import PDA
from .logic import open_bracket, close_bracket


# Create state entities
plus_minus = PDAState(self_transit=True)
mul_div = PDAState()
zero_one = PDAState(self_transit=True)
left_bracket = PDAState(self_transit=True, logic=open_bracket)
right_bracket = PDAState(self_transit=True, logic=close_bracket)

# Add transitions
plus_minus.add_transitions(zero_one, left_bracket)
mul_div.add_transitions(zero_one, left_bracket)
zero_one.add_transitions(plus_minus, mul_div, right_bracket)
left_bracket.add_transitions(plus_minus, zero_one)
right_bracket.add_transitions(plus_minus, mul_div)

# Create PDA object
pda = PDA(states={"+-": plus_minus,
                  "*/": mul_div,
                  "01": zero_one,
                  "(": left_bracket,
                  ")": right_bracket},
          init_states={plus_minus,
                       zero_one,
                       left_bracket},
          term_states={zero_one,
                       right_bracket},
          term_function=lambda stack: len(stack) == 0)


def expression_is_valid(input):
    return pda.run(input)
