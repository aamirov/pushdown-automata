class PDA:

    def __init__(self, states, init_states, term_states, term_function):
        self.states = states
        self.init_states = init_states
        self.term_states = term_states
        self.term_function = term_function

    def resolve_state(self, char):
        for symbol, state in self.states.items():
            if char in symbol:
                return state
        return None

    def run(self, _input):
        stack = []
        output = {'valid': True, 'matches': []}
        false = {'valid': False, 'matches': []}
        if _input == '':
            return output
        cur_state = self.resolve_state(_input[0])
        if cur_state not in self.init_states:
            return false
        i = 0
        for char in _input[1:]:
            if cur_state.has_logic() and not cur_state.execute_logic(stack, output['matches'], i):
                return false
            if not cur_state.can_transit(self.resolve_state(char)):
                return false
            cur_state = self.resolve_state(char)
            i += 1
        if cur_state.has_logic() and not cur_state.execute_logic(stack, output['matches'], i):
            return false
        output['valid'] = self.term_function(stack)
        if not output['valid'] or cur_state not in self.term_states:
            return false
        output['matches'] = sorted(output['matches'])
        return output
